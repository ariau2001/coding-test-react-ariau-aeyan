import React, { Component } from 'react'

export class Dropdown extends Component {
    constructor(props) {
        super(props);  
        this.state = {
            className: "dropdown"
        }
        this.handleDropdownChange = this.handleDropdownChange.bind(this);
      }
    
      handleDropdownChange(e) {
        this.props.onChangeCall(e.target.value);
      }
      
    render() {
        return (
            <select onChange={this.handleDropdownChange} className={this.state.className}>
                {
                    this.props.dropdownList.map(
                        user => <option key={user.id}
                        value={user.id}>{user.name}</option>)
                } 
            </select>
        )
    }
}
export default Dropdown
