import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";
import Icon from '@material-ui/core/Icon';
import Dropdown  from '../components';
import { title } from 'utils';
import { userService } from 'services';


class UserPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            id: 1,
            user: {},
            completed: false
        }
    }

    componentDidMount() {
        // userService.list() ... Pour remplir this.state.list
        userService.list().then(
            (data) => {
                this.setState({ 
                    list: data
                });
            },
            userService.userInfo(this.state.id).then(
                userInfo => this.setState({user: userInfo, id: userInfo.id, completed: true})
            )
        );
    }

    onSelectChange = async (selectedValue) => {
        await this.props.history.push(`/users/${selectedValue}`);
        const {match: {params}} = this.props;
        this.setState({
            id: params.id,
        }, () => {
            userService.userInfo(this.state.id).then(
                userInfo => this.setState({user: userInfo})
            );
        });
    }


    render() {
        let articlesList = this.loadArticles();
        return (
            
            <Fragment>
                <Helmet>
                    { title('Page secondaire') }
                </Helmet>

                <div className="user-page content-wrap">
                    <Link to="/" className="nav-arrow">
                        <Icon style={{ transform: 'rotate(180deg)' }}>arrow_right_alt</Icon>
                    </Link>

                    <div className="users-select">
                        <h1>
                            <Dropdown dropdownList={this.state.list} onChangeCall={this.onSelectChange}/>
                        </h1>
                    </div>

                    <div className="infos-block">
                        { /* Infos dynamiques sur l'utilisateur sélectionné */ }
                        <h3 className="info">Nom: <span>{this.state.user.name}</span></h3>
                        <h3 className="info">Occupation: <span>{this.state.user.occupation}</span></h3>
                    </div>

                    <div className="articles-list">
                        { /* Liste dynamique tirée de l'utilisateur sélectionné */ }
                        {articlesList}
                        
                    </div>
                </div>
            </Fragment>
        )
    }

    loadArticles() {
        let articlesList = null;
        if (this.state.completed) {
            articlesList = this.state.user.articles.map(article => {
                return (<div key={`article${article.id}`} className="article">
                    <div className="article_title">{article.name}</div>
                    <p className="article_content">
                        {article.content}
                    </p>
                </div>);
            });
        }
        return articlesList;
    }
}

export default UserPage;
