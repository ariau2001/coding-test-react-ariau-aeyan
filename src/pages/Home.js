import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";
import Icon from '@material-ui/core/Icon';

import { title } from 'utils';


class HomePage extends Component {
    render() {
        return (
            <Fragment>
                <Helmet>
                    { title('Page d\'accueil') }
                </Helmet>

                <div className="home-page content-wrap"> 
                    
                    <div className="block_title">
                        <div className="title">                        
                            <h1>04h11</h1>
                            <span className="title_outline">04h11</span>
                        </div>
                        <div className="sub_title">
                            <p>Spécialiste de vos données.</p>
                        </div>
                    </div>

                    <div className="block_arrow">
                        <Link to="/users" className="nav-arrow">
                            <Icon>arrow_right_alt</Icon>
                        </Link>
                    </div>
                    
                    <div className="circle circle_left"></div>
                    <div className="circle circle_right"></div> 
                </div>
            </Fragment>
        )
    }
}

export default HomePage;
